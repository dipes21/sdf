import React, { Component } from "react";
import {
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  AsyncStorage,
  Image,
  ScrollView,
 // ToastAndroid
} from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";
import { Actions } from "react-native-router-flux";
import { connect } from 'react-redux';

import * as drawerAction from '../actions'
import I18n from '../translations/i18n';
import LanguagePicker from './common/inputs/LanguagePicker';
import Colors from './common/Colors';
import { bindActionCreators } from 'redux';
class DrawerMenu extends Component {

  constructor(){
    super();
    this.state={
      user:'',
        image:''
    }
  }
componentDidMount()
{
   // //console.log('drawer component',this.props.id);
    this.props.actions.userDetail(this.props.id);


    //this.setState({'image': this.props.detail?this.props.detail.umva_photo_jpg:''})
}
  logout() {
    //console.log("This is logout",this.props);
    this.props.actions.logout();
    Actions.pop();
   // Actions.login();
  }
    // tbusinessiveProps(nextProps) {
    //     //console.log(nextProps.lang, "next");
    //     I18n.locale = nextProps.lang;
    //
    // }
  payPressed(){
    //console.log("Pressed")
    // //console.log(this.props.id)
    Actions.pay();

}
    receivePressed() {
        //console.log("Pressed")
        // this.props.merchantAccounts(this.props.id, "pay");
        Actions.receive();

    }
showAlert(){
//  ToastAndroid.show('Under Construction', ToastAndroid.SHORT);
}

qrpay(qrType){
  this.props.actions.userAccount(this.props.id,qrType,'Merchant');
}
  render() {

    return (
      <ScrollView style={styles.container}>
        <View style={{ backgroundColor: Colors.redhishOrange, justifyContent: 'center', alignItems: 'center' }}>
          <Image source={this.props.detail?this.props.detail.umva_photo_jpg_pending?"data:image/png;base64, "+this.props.detail.umva_photo_jpg_pending: require('../images/logo.png'):require('../images/logo.png')} style={{
            width: 80,
            height: 80,
            borderRadius: 80,
            marginTop: 5,
            marginBottom: 10
          }} />

          <Text
            style={{
              fontSize: 15,
              color: 'white',
              marginLeft: 10,
              marginBottom: 10
            }}

          >{this.props.umva_id}</Text>

        </View>
          <LanguagePicker selectedValue={this.props.lang} icon={"language"} iconcolor={Colors.redhishOrange} />
        <TouchableOpacity
          style={styles.menuItem}
          onPress={Actions.index}
        >
          <Icon name="home" size={30} color={Colors.redhishOrange} style={{ marginRight: 5 }} />

          <Text style={styles.menuItemText}>{I18n.t('Home')}</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.menuItem}
          onPress={this.payPressed.bind(this)}
        >
          <Icon name="payment" size={30}  color={Colors.redhishOrange} style={{ marginRight: 5 }} />

          <Text style={styles.menuItemText}>{I18n.t("Payment")}</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.menuItem}
          onPress={this.qrpay.bind(this)}
        >
          <Icon name="forward" size={30}  color={Colors.redhishOrange} style={{ marginRight: 5 }} />

          <Text style={styles.menuItemText}>{I18n.t("QR Payment")}</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={styles.menuItem}
          onPress={()=>this.qrpay('qr')}
        >
          <Icon name="forward" size={30}  color={Colors.redhishOrange} style={{ marginRight: 5 }} />

          <Text style={styles.menuItemText}>{I18n.t("Transaction")}</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.menuItem} onPress={()=>this.logout()}>
          <Icon name="backspace" size={30}  color={Colors.redhishOrange} style={{ marginRight: 5 }} />
          <Text style={styles.menuItemText}>{I18n.t("Logout")}</Text>
        </TouchableOpacity>

      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {


  },
  menuItem: {
    padding: 7,
    // backgroundColor: "ivory",
    marginBottom: 2,
    margin: 5,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  menuItemText: {
    fontSize: 18,
    color: Colors.redhishOrange,
    paddingTop: 2
  }
});

DrawerMenu.defaultProps = {};

DrawerMenu.propTypes = {};

const mapStateToProps = (state) => {
  //console.log("from drawer",state);

  return {
    balance: state.auth.balance,
    id:state.auth.user.user_id,
      umva_id:state.auth.umva_id,
      lang: state.language.language,
      detail:state.user.detail,

  }
};

function mapDispatchtoProps(dispatch) {
    return{
        actions: bindActionCreators(drawerAction,dispatch),
    }

}
export default connect(mapStateToProps, mapDispatchtoProps)(DrawerMenu);
