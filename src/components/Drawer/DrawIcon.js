import React from 'react';
import {View} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome'
const DrawIcon=()=>{

    return(
        <View>
            <Icon name={"bars"} style={{fontSize:21}} color={"#ffffff"}></Icon>
        </View>
    )


}

export default DrawIcon;
