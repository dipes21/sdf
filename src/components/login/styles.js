import {StyleSheet} from 'react-native'
import { ResponsiveStyleSheet } from "react-native-responsive-ui";
import Colors from "../common/Utility/Colors";

const stylesForm = StyleSheet.create({
    container: {
        width: '80%'

    },


    button: {
        padding: 10,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#ffffe0',
        backgroundColor: '#ff6f00',
        // elevation: 1,
        alignSelf: 'flex-start',
        width: '50%',
        marginTop: 10
    }
});

const sytleLayout= ResponsiveStyleSheet.select(
    [
        {
            query:{orientation:'landscape'},
            style:{
                container:{
                    flex:1,
                    flexDirection:'row'
                },
                logoContainer:{
                    flex:1,
                    width:"50%",
                    backgroundColor: Colors.orange,
                    justifyContent: 'center',
                    alignItems: 'center'
                },
                img:{
                    width:192,
                    height:192,
                    // / borderRadius:'50%',
                    margin:10
                },
                formContainer:
                    {
                        flex:1,
                        width:'50%',
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor:'#f5f5f5'

                    }
            }
        },
        {
            query:{orientation:'portrait'},
            style:{
                container:{
                    flex:1,
                    flexDirection:'column'
                },
                logoContainer:{
                    flex:0.4,
                    width:"100%",

                    backgroundColor: Colors.orange,
                    justifyContent:"center",
                    alignItems:"center"
                },
                img:{
                    width:100,
                    height:100,
                    // / borderRadius:'50%',
                    //  margin:'1em'
                },
                formContainer:
                    {
                        flex:0.6,

                        width:'100%',

                        justifyContent:'center',
                        alignItems:'center',
                        backgroundColor:'#f5f5f5'
                    }
            }
        }

    ])
export {stylesForm,sytleLayout}