import React from 'react';
import { View, ActivityIndicator } from 'react-native';

const Spinner = ({ size }) => {
    return (
        <View style={styles.spinnerStyle}>
            <ActivityIndicator size={size || 'large'} />
        </View>
    );
};


const styles = {
    spinnerStyle: {
        position:'absolute',
        zIndex:9999,
        width:'100%',
        height:'100%',
        backgroundColor:'rgba(255,255,255,0.5)'
    }
};

export default Spinner ;
