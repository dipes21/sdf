export default Colors={
    darkestOrange: '#B44010',
    darkOrange: '#E55300',
    orange: '#FF6900',
    redhishOrange:'#FF2B00',
    dullRedishOrange: '#FF4023',
    lightRedishOrange: '#FF5C00'
}