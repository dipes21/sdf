import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {StyleSheet, View, Picker,Dimensions, Text} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {languageUpdate} from '../../../actions/index';
import {connect} from "react-redux";
import * as languages from '../../../translations/langs';
import styles from './styles';
import I18n from '../../../translations/i18n'
 class LanguagePicker extends Component {
    constructor(props)
    {
        super();
        this.state={
            languages:Object.keys(languages)
        }

    }

    changeLanguage(value)
    {
        this.props.languageUpdate(value);
        this.setState({language: value})
    }
    render() {

        return (
            <View style={{marginBottom:20,height:50}}>
                {this.props.errortext ? <Text style={{color:'#ff0000',fontSize:16,marginBottom:10,paddingLeft:30}}><Icon name={'warning'} color={'#ff0000'}></Icon>{I18n.t(this.props.errortext)} </Text>:null }
            <View style={styles.container}>
                <View style={styles.iconWrapper}><Icon name={this.props.icon} style={styles.icon} color={this.props.iconcolor} /></View>
                <View style={styles.inputWrapper}>
                <Picker

                   selectedValue={this.props.lang}
                    style={styles.input}
                   onValueChange={(itemValue)=>this.changeLanguage(itemValue)} >
                    {Object.keys(languages).map((item,index)=> {

                            return <Picker.Item style={styles.items} key={index} label={languages[item].label} value={item}/>
                        }
                    )}

                </Picker>
                </View>
            </View>
                </View>


        );
    }
}

LanguagePicker.propTypes = {
    icon: PropTypes.string,
    iconcolor: PropTypes.string,
    errortext: PropTypes.string

};

const mapStateToProps = (state) => {

    return {
        lang: state.language.language
    };
};

export default connect(mapStateToProps, {  languageUpdate })(LanguagePicker);

