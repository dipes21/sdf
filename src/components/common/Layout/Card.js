import React,{Component} from 'react';
import { View, Text,StyleSheet } from 'react-native';
import Colors from "../Utility/Colors";
import PropTypes from 'prop-types';
//import KeyboardSpacer from 'react-native-keyboard-spacer';
class Card extends Component{
    constructor(props)
    {
        super();

    }
    render(){
        return(
            <View style={[this.styles.containerStyle,this.props.style]}>
                {this.props.title?<View style={this.styles.header}>
                    <Text style={this.styles.title}>{this.props.title}</Text>
                </View>:null}
                <View style={this.styles.body}>
                {this.props.children}
                </View>

            </View>
        );
    }

    get styles(){
        return StyleSheet.create(
            {
                containerStyle: {
                    borderWidth: 1,
                    borderRadius: 2,
                    borderColor: '#ddd',
                    borderBottomWidth: 0,
                    shadowColor: '#000',
                    shadowOffset: { width: 0, height: 2 },
                    shadowOpacity: 0.1,
                    shadowRadius: 2,
                    margin:5,
                    padding:5,
                    backgroundColor:'#ffffff',
                    flex:1,
                    flexDirection:'column'

                },
                header:{
                   height:25,

                    borderBottomWidth:1,
                    borderBottomColor: Colors.redhishOrange,

                },
                title:{
                    color:Colors.darkestOrange,
                    fontSize:20                },

                body:
                    {

                        flex:1
                    }
            }
        )
    }

}
Card.propTypes = {
   title: PropTypes.string
};

export default Card;
