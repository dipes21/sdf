import {AccountActionTypes} from "../actionTypes";
import {call, put, take,select} from "redux-saga/es/effects";
import {AccountService} from "../services/AccountService";
const getState=(state)=>state;
export function* watchUserAccountsMerchant() {

    while (true) {
        const { id, value } = yield take(AccountActionTypes.MERCHANT_ACCOUNTS_REQUEST);
        const payload = {
            user_id:id};
        const accounts=yield call(AccountService.getAccounts, payload);
        yield put ({type:AccountActionTypes.MERCHANT_ACCOUNTS_SUCCESS,merchantAccounts:accounts,loading:false})
        const state=yield select(getState);
        const defaultAccount=state.auth.user.default_account_id;
        var result=accounts.find(obj=>obj.account_id==defaultAccount);

        yield put ({type:AccountActionTypes.MERCHANT_ACCOUNT_SELECTED,selectedMerchant:result?result:accounts[0],loading:false})
       // yield put({type:PaymentActionTypes.PAYMENT_LIST_REQUEST,account_id:result.account_id,umva_id:state.user.detail.umva_id,value_type_id:result.value_type_id})
    }
}

export function* watchMerchantAccountChange() {

    while (true) {
        const {payload} = yield take(AccountActionTypes.MERCHANT_ACCOUNT_CHANGE_REQUEST);


        //const accounts=yield call(AccountService.getAccounts, payload);
        yield put ({type:AccountActionTypes.MERCHANT_ACCOUNT_SELECTED,selectedMerchant:payload})

    }
}

export function* watchUserAccountsClient() {

    while (true) {
        const { id, value } = yield take(AccountActionTypes.CLIENT_ACCOUNTS_REQUEST);
        const payload = {
            user_id:id};

        const accounts=yield call(AccountService.getAccounts, payload);
        const state=yield select(getState);
       const merchantBank=state.accounts.selectedMerchant?state.accounts.selectedMerchant.bank_name:null;


           var result=accounts.find(obj=>obj.bank_name==merchantBank);
           if(!result)
           {
               result={account_id:'',
                   bank_id:'',
                   bank_name:'',
                   bankaccount:'',
                   value_type:'',
                   value_type_id:''
               }
           }


        yield put ({type:AccountActionTypes.CLIENT_ACCOUNTS_SUCCESS,clientAccounts:[result]})



        yield put ({type:AccountActionTypes.CLIENT_ACCOUNT_SELECTED,selectedClient:result})

    }
}

export function* watchClientAccountChange() {

    while (true) {
        const {payload} = yield take(AccountActionTypes.CLIENT_ACCOUNT_CHANGE_REQUEST);


        yield put ({type:AccountActionTypes.CLIENT_ACCOUNT_SELECTED,selectedClient:payload})

    }
}

