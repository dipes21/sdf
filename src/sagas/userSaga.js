import {AccountActionTypes, UserActionTypes} from "../actionTypes";
import {AuthService} from "../services/AuthService";
import { put, takeLatest, take, call } from 'redux-saga/effects';
import {UserService} from "../services/UserService";


export function* watchUserDetail()
{
    while(true)
    {
        const {user_id}=yield take(UserActionTypes.USER_DETAIL_REQUEST)
        const payload={
            "user_id":user_id
        }

        const detail=yield call(AuthService.userFromUserId,payload);
        yield put({type:UserActionTypes.USER_DETAILS,detail:detail,loading:false})
        yield put( {type:AccountActionTypes.MERCHANT_ACCOUNTS_REQUEST,id:detail.userid})




    }
}
