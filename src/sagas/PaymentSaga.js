import {AccountActionTypes, PaymentActionTypes,UserActionTypes} from "../actionTypes";
import { Actions} from 'react-native-router-flux';
//Saga effects
import { put, take, call,select } from 'redux-saga/effects';
import {PaymentService} from "../services/PaymentService";
import {AuthService} from '../services/AuthService';
import {AccountService} from "../services/AccountService";


export const getState=(state)=>state;



export function* watchSendPayments() {

    while (true) {
        const gState=yield select(getState);

        const { accounts, recipient, description, amount,receive } = yield take(PaymentActionTypes.SEND_PAYMENTS_REQUEST);

        let status="error";

        if(!recipient)
        {
           if(receive)
            yield put({type:PaymentActionTypes.SEND_PAYMENT_FAIL,error:{recipient:'Invalid Sender',status}})
            else
               yield put({type:PaymentActionTypes.SEND_PAYMENT_FAIL,error:{recipient:'Invalid Recipient',status}})
        }
        else if(recipient==gState.auth.umva_id)
        {
            yield put({type:PaymentActionTypes.SEND_PAYMENT_FAIL,error:{recipient:'cannot send to self',status}})
        }
        else if(!accounts)
        {
            yield put({type:PaymentActionTypes.SEND_PAYMENT_FAIL,error:{Merchant:'Invalid Account',status}})
        }
        else if(!amount)
        {
            yield put({type:PaymentActionTypes.SEND_PAYMENT_FAIL,error:{amount:'Amount cannot be empty',status}})
        }
        // else if(amount>accounts.currentbalance)
        // {
        //     yield put({type:PaymentActionTypes.SEND_PAYMENT_FAIL,error:{amount:'insufficient Balance',status}})
        // }
        else if(!description)
        {
            yield put({type:PaymentActionTypes.SEND_PAYMENT_FAIL,error:{description:'description cannot be empty',status}})
        }
        else if(description.length<3)
        {
            yield put({type:PaymentActionTypes.SEND_PAYMENT_FAIL,error:{description:'description cannot be less than 3 letter',status}})
        }
        else
        {
            var payload={};
            if(receive)
            {

                const recipient_id = yield AuthService.userFromUserId({user_id:recipient});

                yield put({type:UserActionTypes.USER_DETAILS,detail:recipient_id,recipient:true})
                const clientAccounts=yield AccountService.getAccounts({user_id:recipient_id.userid})

                const gState=yield select(getState);

                const clientAccount=clientAccounts.find(obj=>obj.bank_name===gState.accounts.selectedMerchant.bank_name);

              yield put({type:AccountActionTypes.CLIENT_ACCOUNT_CHANGE_REQUEST,payload:clientAccount})
               payload={
                  'from_account_id': clientAccount.account_id,
                  'to_account_id': gState.accounts.selectedMerchant.account_id,
                  'amount': amount,
                  'receiver_login': gState.auth.umva_id,
                  'description':description
              }

                const user=yield call(PaymentService.sendPaymentsFromApi, payload);

                if (!user.status) {

                    let amount = user.amount;
                    let recipient = user.recipient;
                    let description = user.description;
                    let user_acc = user.accounts;
                    yield put({ type: PaymentActionTypes.PAYMENT_CHECK_SUCCESS, amount, recipient, description, user_acc,recipient_id });
                    yield call(Actions.confirmReceive);


                }
                else {
                    let error = user;
                    yield put({ type: PaymentActionTypes.SEND_PAYMENT_FAIL, error });

                }
//
            }
            else
            {
                payload = {

                    'from_account_id': accounts.account_id,
                    'to_account_id': null,
                    'amount': amount,
                    'receiver_login': recipient,
                    'description':description
                };

                const user=yield call(PaymentService.sendPaymentsFromApi, payload);

                const recipient_id = yield AuthService.userFromUserId({user_id:recipient});
                if (!user.status) {

                    let amount = user.amount;
                    let recipient = user.recipient;
                    let description = user.description;
                    let user_acc = user.accounts;
                    yield put({ type: PaymentActionTypes.PAYMENT_CHECK_SUCCESS, amount, recipient, description, user_acc,recipient_id });
                    yield call(Actions.confirm);


                }
                else {
                    let error = user;
                    yield put({ type: PaymentActionTypes.SEND_PAYMENT_FAIL, error });

                }
//
            }



        }


    }
}

export function* watchConfirmRequest() {

    while (true) {
        let status='error';
        
        const { account, recipient, description, amount, faccount,recieve,password } = yield take(PaymentActionTypes.CONFIRM_REQUEST);
        if(recieve)
        {
            const state= yield select(getState);
            const userClient= yield call(AuthService.userLoginFromApi,{umva_id:state.user.clientDetail.umva_id,password:password});
            if(userClient.status)
            {
                yield put({type:PaymentActionTypes.SEND_PAYMENT_FAIL,error:userClient})
                yield call(Actions.confirmReceive);
            }
            else{
                const payload = { account, recipient, description, amount, faccount }
        const state= yield select(getState);
        if(state.accounts.selectedMerchant.bank_name===state.accounts.selectedClient.bank_name) {
            const result = PaymentService.confirmPayment(payload.account, payload.recipient, payload.description, payload.amount, payload.faccount);
            if (!result.status) {

                yield put({type: PaymentActionTypes.PAYMENT_SUCCESS});

                yield call(Actions.index);

            }
            else {
                let error = result.message;
                let code = result.status
                yield put({type:PaymentActionTypes.SEND_PAYMENT_FAIL, error, code});

            }
        }
        else
        {
            yield put({type:PaymentActionTypes.SEND_PAYMENT_FAIL,error:{account:'banks should be same',status}})
        }
            }
        }
        else{
            const payload = { account, recipient, description, amount, faccount }
            const state= yield select(getState);
            if(state.accounts.selectedMerchant.bank_name===state.accounts.selectedClient.bank_name) {
                const result = PaymentService.confirmPayment(payload.account, payload.recipient, payload.description, payload.amount, payload.faccount);

                if (!result.status) {

                    yield put({type: PaymentActionTypes.PAYMENT_SUCCESS});
    
                    yield call(Actions.index);

    
                }
                else {
                    let error = result.message;
                    let code = result.status
                    yield put({type:PaymentActionTypes.SEND_PAYMENT_FAIL, error, code});
    
                }
            }
            else
            {
                yield put({type:PaymentActionTypes.SEND_PAYMENT_FAIL,error:{account:'banks should be same',status}})
            }
        }
        

    }
}

export function* watchQrPyament() {

    while (true) {
        const { id, value,clientType } = yield take(PaymentActionTypes.QR_PAYMENT_REQUEST);
        const payload = { id, value };
       if(value=='qr' || value=='qr-receive')
       {
           yield call(Actions.qr);
           const {payload}=yield take(PaymentActionTypes.QR_REQUEST)
           yield put({type:PaymentActionTypes.QR_SUCCESS,recipient:payload,qrType:value})

       }
       else
       {
           Actions.index();
       }


    }
}
export function* watchPaymentList()
{
    while(true)
    {
        const { account_id, umva_id,value_type_id } = yield take(PaymentActionTypes.PAYMENT_LIST_REQUEST);
        const state=yield select(getState)
        const payload = {
            "account_id":state.accounts.selectedMerchant.account_id,
            "umva_id":state.user.detail.umva_id,
            "value_type_id":state.accounts.selectedMerchant.value_type_id
        }
        const list=yield call(PaymentService.paymentList,payload)

        if(list.status)
        {
            yield put({ type: PaymentActionTypes.PAYMENT_LIST_FAIL , error:list, loading: false });
        }
        else {
            yield put({ type: PaymentActionTypes.PAYMENT_LIST_SUCCESS, list: list.data,loading: false });
            //  act.index();

        }
    }
}
