import {Api} from './Api';


function userLoginFromApi(payload) {
    const data=  Api.post('login',payload).then((response)=>response).then((responseJson) => {
        /*if (response.data.code === 200) {
            return response.data.result;
        }*/
        if (responseJson.data.code === 200) {
            let result = responseJson.data.result;
            return result;
        }
        else {
            let status = "error";
            switch(responseJson.data.error.code)
            {
                case 300006:

                    return {umva_id:responseJson.data.error.message,status}
                case 300001:

                    return {umva_id:responseJson.data.error.message,status}
                case 300002:

                    return {password:responseJson.data.error.message,status}

                default:
                    let error=responseJson.data.error.message;
                    return {error,status}
            }
            // let error = responseJson.data.error.message;
            // let status = "error";
            // return { error, status }
        }
    }).catch((error)=>{
        return error;
    });
return data;
}

function userFromUserId(params)
{

    const res = Api.get(
        'userInfo',
        {params}
    ).then((response) => {

            return response.data.result;
        }
    ).catch(function (e) {
        let error = "Network Failed"
        return error
    });
    return res;
}

export const AuthService={
    userLoginFromApi,
    userFromUserId
}