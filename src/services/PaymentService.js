import {Api} from "./Api";


function sendPaymentsFromApi(params) {


    const res = Api.post(
        'checkPaymentPossible',

            params
            //other data key value pairs

    ).then((response) => {

        if (response.data.code === 200) {

            return { recipient:params.receiver_login, amount:params.amount, description:params.description, accounts:params.from_account_id };

        }
        else {
            let status='error';

                    return {general:response.data.error.message,status}


        }

    })
        .catch(function (e) {
            let error = "Network Failed"

        });
    return res;

}
function confirmPayment(account, recipient, description, amount, faccount) {

    const res = Api.post(
        'paymentToOther',
        {
            'from_account_id': faccount,
            'to_account_id': account,
            'amount': amount,
            'receiver_login': recipient,
            'description': description
            //other data key value pairs
        }
    ).then((response) => {

        if (response.data.status === "success") {
            return "success";
        }
        else {
            let status = response.data.error.code;
            let message= response.data.error.message;
            return {status,message};
        }

    });

    return res;


}

function paymentList(params)
{
    const res= Api.get(
        'paymentOperationList',
        {
           params
        }
    ).then((response) => {

            return response.data.result;
        }
    ).catch(function (e) {
        let error = "Network Failed"
        return error
    });
    return res;
}

export const PaymentService={
    sendPaymentsFromApi,
    confirmPayment,
    paymentList
}
