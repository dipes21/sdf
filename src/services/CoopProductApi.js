import axios from 'axios';

export const CoopProductApi=axios.create(
    {
        baseURL: 'http://test-coop.umva.org/api/',
        timeout:12000,
        headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjI3NTczMDllZjNkZjNkMmQwNjhkOGYxZGUwNTFjN2RiZWU0NWU0NmExNDk4ZGM1NWM1YWRiMmRhMDJmMDFjMGZiNWI0NzgxOWRmNDU5OTQzIn0.eyJhdWQiOiIyIiwianRpIjoiMjc1NzMwOWVmM2RmM2QyZDA2OGQ4ZjFkZTA1MWM3ZGJlZTQ1ZTQ2YTE0OThkYzU1YzVhZGIyZGEwMmYwMWMwZmI1YjQ3ODE5ZGY0NTk5NDMiLCJpYXQiOjE1Mzg1NTk3NTMsIm5iZiI6MTUzODU1OTc1MywiZXhwIjoxNTcwMDk1NzUzLCJzdWIiOiIxIiwic2NvcGVzIjpbXX0.ZUB04thDzXHXMZRLu2NSt6HBuOMTWJjm2c7TzF98w3HU8tIFm3wehwKjtj1B5Nl5mTluVJzvJIPBgIPJv6RJLgrXJ2_rD9qiKLoBo6d-lYB1gaOqRACrNjAlufddVWNESq9vPqzC4GOYAR1DSwHg0RPh4eI3-n8FAy8v79J98E7EUEBJt8yTPMdqfHxiLpgN6xgDqG_tGWXAEB3AYFHUH961kBn8P69ZoGmk3EsBgvdhcYAEgDIhfN1qaoFqg1SJZhK73LgPMgrHL5rWvn1D9TupWYh_whplFBPx67R0kLRA-wcCl4cyn5uQ48Nq5x2uxYcxGqBnKIL2rhfyb6m24gVqjZoy8jiMK4ns5ledw7myfzP4ZVOYCCSpYHvArBv_aMn8nMZLppLR69Y6aOzObU6el6iZ1e4aRe1sdEym3hi_TGhgwt876zbaaKEa8BiMefWJ1wY4mZ5C32ZMks4RYNGgQ6mELURKWZlIjJ-CMs7Xcr4TJKbWvmWzSkxHCwaIwfL0Xywf_Q75SxzmBJcKHUfqlu_vCQNlquIGbELyqAEQEoXFlBmhq-s7-kQPagi-rMA5zwpJaTmdDe2COiFS3ona6zCv2qUDqYFa9Y2Ox8V1LjjQvS8hbgkcLpeqDS56xLmI0o18PzrmsEJiLRZ60oBlTuqsd4tkN8l11JrjVqA',
        }

    }
)
