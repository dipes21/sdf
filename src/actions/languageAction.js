import {LanguageActionTypes} from '../actionTypes'

export function languageUpdate(language)
{

    return{
        type: LanguageActionTypes.LANGUAGE_UPDATE,
        language:language
    }

}